import sys
import random
import signal

class Player57:
	
	def __init__(self):
		self.levelmax = 2
		self.movecall = 0
		pass

	#Gets empty cells from the list of possible blocks. Hence gets valid moves. 
	def get_empty_out_of(self,gameb, blal,block_stat):
		cells = []  # it will be list of tuples
		#Iterate over possible blocks and get empty cells
		for idb in blal:
			id1 = idb/3
			id2 = idb%3
			for i in range(id1*3,id1*3+3):
				for j in range(id2*3,id2*3+3):
					if gameb[i][j] == '-':
						cells.append((i,j))

		# If all the possible blocks are full, you can move anywhere
		if cells == []:
			for i in range(9):
				for j in range(9):
	                                no = (i/3)*3
	                                no += (j/3)
					if gameb[i][j] == '-' and block_stat[no] == '-':
						cells.append((i,j))	
		return cells

	def checkwin(self,board,cell,flag_player):
		x = cell[0]
		y = cell[1]
		#print cell
		if flag_player == 1:
			board[x][y] = 'x'
		else:
			board[x][y] = 'o'

		if ((0<=y) and (2>=y) and (0<=x) and (2>=x)):
			idb = 0
		if ((3<=y) and (5>=y) and (0<=x) and (2>=x)):
			idb = 1
		if ((6<=y) and (8>=y) and (0<=x) and (2>=x)):
			idb = 2
		if ((0<=y) and (2>=y) and (3<=x) and (5>=x)):
			idb = 3
		if ((3<=y) and (5>=y) and (3<=x) and (5>=x)):
			idb = 4
		if ((6<=y) and (8>=y) and (3<=x) and (5>=x)):
			idb = 5
		if ((0<=y) and (2>=y) and (6<=x) and (8>=x)):
			idb = 6
		if ((3<=y) and (5>=y) and (6<=x) and (8>=x)):
			idb = 7
		if ((6<=y) and (8>=y) and (6<=x) and (8>=x)):
			idb = 8

		id1 = idb/3 #0
		id2 = idb%3 #1

		id1 = id1*3;
		id2 = id2*3;
		
		x = cell[0]%3 #2
		y = cell[1]%3 #2
		
		if board[id1+0][id2+y] == 'x' and board[id1+1][id2+y] == 'x' and board[id1+2][id2+y] == 'x':
			return True
		if board[id1+0][id2+y] == 'o' and board[id1+1][id2+y] =='o' and board[id1+2][id2+y] == 'o':
			return True

	    #check if previous move was on horizontal line and caused a win
		if board[id1+x][id2+0] == 'x' and board[id1+x][id2+1] == 'x' and board[id1+x][id2+2] == 'x':
			return True
		if board[id1+x][id2+0] == 'o' and board[id1+x][id2+1] == 'o' and board[id1+x][id2+2] == 'o':
			return True

	    #check if previous move was on the main diagonal and caused a win
		if board[id1+0][id2+0] == 'x' and board[id1+1][id2+1] == 'x' and board[id1+2][id2+2] == 'x':
			return True
		if board[id1+0][id2+0] == 'o' and board[id1+1][id2+1] == 'o' and board[id1+2][id2+2] == 'o':
			return True
	    #check if previous move was on the secondary diagonal and caused a win
		if board[id1+0][id2+2] == 'x' and board[id1+1][id2+1] == 'x' and board[id1+2][id2+0] == 'x':
			return True
		if board[id1+0][id2+2] == 'o' and board[id1+1][id2+1] == 'o' and board[id1+2][id2+0] == 'o':
			return True

		return False

	#gameb-board
	def check_available_cell(self,gameb,block,temp_block):
		for idb in block:
			id1 = idb/3
			id2 = idb%3
			for i in range(id1*3,id1*3+3):
				for j in range(id2*3,id2*3+3):
					if gameb[i][j] == '-' and temp_block[idb] == '-':
						return True
		return False

	### - AI CALLS AND UDF Functions
	def AICALL(self,temp_board, blocks_allowed, temp_block, player_no, level):
		if level > self.levelmax:
			return

		if level == 1:
			heuristic_dict = {}
		
		# Cells loctions in center
		center_cells = [(1,1),(1,4),(1,7),(4,1),(4,4),(4,7),(7,1),(7,4),(7,7)]
		
		# Cells loctions in corner
		corner_cells = [(0,0),(0,2),(0,3),(0,5),(0,6),(0,8),
						(2,0),(2,2),(2,3),(2,5),(2,6),(2,8),
						(3,0),(3,2),(3,3),(3,5),(3,6),(3,8),
						(5,0),(5,2),(5,3),(5,5),(5,6),(5,8),
						(6,0),(6,2),(6,3),(6,5),(6,6),(6,8),
						(8,0),(8,2),(8,3),(8,5),(8,6),(8,8)]
		
		# temp variable for change
		from copy import deepcopy
		total_blocks = deepcopy(blocks_allowed)
		
		# Removing the already-won blocks from available blocks
		for i in range(len(total_blocks)):
			if temp_block[total_blocks[i]] != '-':
				blocks_allowed.remove(total_blocks[i])
		
		# getting cells from available blocks
		cells = self.get_empty_out_of(temp_board,blocks_allowed,temp_block)
		
		# Initial heuristics
		heuristic = -100
		try:
			final_cell = cells[0]
		except:
			return heuristic
		
		middle_available = []
		center_available = []
		
		if level == 1:
			if len(cells) == 1:
				return cells[0]
		
		# Node Expansion
		for each_cell in cells:
			# copies temporary boards and blocks
			another_board = deepcopy(temp_board)
			another_block = deepcopy(temp_block)
			
			# Assigns Heuristics
			# Game completely won
			if self.game_win(another_board,another_block,each_cell,player_no):
				temp_heuristic = 100
				if level == 1:
					return each_cell
			else:
				another_board = deepcopy(temp_board)
				# Block Won
				if self.checkwin(another_board,each_cell,player_no):
					temp_heuristic = 60
				else: 
					another_board = deepcopy(temp_board)
					if player_no == 1:
						player_no = 2
					else:
						player_no = 1
					# Opponent Blocking
					if self.checkwin(another_board,each_cell,player_no):
						temp_heuristic = 40
					else:
						# Preference to Center
						if each_cell in center_cells:
							temp_heuristic = 15
							center_available.append(each_cell)
						# Second to middle blocks
						if each_cell in corner_cells:
							temp_heuristic = 5
						# Corner Blocks Last
						if each_cell not in center_cells and each_cell not in corner_cells:
							temp_heuristic = 10
							middle_available.append(each_cell)
			
			# Assigns Best Heurictics and cell	
			if level < self.levelmax:
				another_board = deepcopy(temp_board)
				another_block = deepcopy(temp_block)
				next_move_blocks = self.virtual_move(another_board,another_block,each_cell)
				if self.checkwin(another_board,each_cell,player_no):
					x = each_cell[0]
					y = each_cell[1]
					if ((0<=y) and (2>=y) and (0<=x) and (2>=x)):
						idb = 0
					if ((3<=y) and (5>=y) and (0<=x) and (2>=x)):
						idb = 1
					if ((6<=y) and (8>=y) and (0<=x) and (2>=x)):
						idb = 2
					if ((0<=y) and (2>=y) and (3<=x) and (5>=x)):
						idb = 3
					if ((3<=y) and (5>=y) and (3<=x) and (5>=x)):
						idb = 4
					if ((6<=y) and (8>=y) and (3<=x) and (5>=x)):
						idb = 5
					if ((0<=y) and (2>=y) and (6<=x) and (8>=x)):
						idb = 6
					if ((3<=y) and (5>=y) and (6<=x) and (8>=x)):
						idb = 7
					if ((6<=y) and (8>=y) and (6<=x) and (8>=x)):
						idb = 8
					if player_no == 1:
						another_block[idb] = 'x'
					else:
						another_block[idb] = 'o'
				if player_no == 1:
					next_player = 2
					another_board[each_cell[0]][each_cell[1]] = 'x'
				else:
					next_player = 1
					another_board[each_cell[0]][each_cell[1]] = 'o'
				next_heuristic = self.AICALL(another_board, next_move_blocks, another_block, next_player,level+1)
				final_heuristic = temp_heuristic - next_heuristic
			
			if level < self.levelmax:
				if level == 1:
					if final_heuristic not in heuristic_dict.keys():
						heuristic_dict[final_heuristic] = [each_cell]
					else:
						heuristic_dict[final_heuristic].append(each_cell)

				if final_heuristic > heuristic:
					heuristic = final_heuristic
					final_cell = each_cell
			else:
				if temp_heuristic > heuristic:
					heuristic = temp_heuristic
					final_cell = each_cell
		
		if level == 1:
			final_list = heuristic_dict[heuristic]
			return final_list[random.randrange(len(final_list))]
		else:
			return heuristic 

	def virtual_move(self,temp_board,temp_block,old_move):
		for_corner = [0,2,3,5,6,8]

		#List of permitted blocks, based on old move.
		blocks_allowed  = []

		from copy import deepcopy
		another_board = deepcopy(temp_board)

		if old_move[0] in for_corner and old_move[1] in for_corner:
			## we will have 3 representative blocks, to choose from

			if old_move[0] % 3 == 0 and old_move[1] % 3 == 0:
				## top left 3 blocks are allowed
				blocks_allowed = [0, 1, 3]
			elif old_move[0] % 3 == 0 and old_move[1] in [2, 5, 8]:
				## top right 3 blocks are allowed
				blocks_allowed = [1,2,5]
			elif old_move[0] in [2,5, 8] and old_move[1] % 3 == 0:
				## bottom left 3 blocks are allowed
				blocks_allowed  = [3,6,7]
			elif old_move[0] in [2,5,8] and old_move[1] in [2,5,8]:
				### bottom right 3 blocks are allowed
				blocks_allowed = [5,7,8]
			else:
				print "SOMETHING REALLY WEIRD HAPPENED!"
				sys.exit(1)
		else:
		#### we will have only 1 block to choose from (or maybe NONE of them, which calls for a free move)
			if old_move[0] % 3 == 0 and old_move[1] in [1,4,7]:
				## upper-center block
				blocks_allowed = [1]

			elif old_move[0] in [1,4,7] and old_move[1] % 3 == 0:
				## middle-left block
				blocks_allowed = [3]
			
			elif old_move[0] in [2,5,8] and old_move[1] in [1,4,7]:
				## lower-center block
				blocks_allowed = [7]

			elif old_move[0] in [1,4,7] and old_move[1] in [2,5,8]:
				## middle-right block
				blocks_allowed = [5]
			elif old_move[0] in [1,4,7] and old_move[1] in [1,4,7]:
				blocks_allowed = [4]

		if self.check_available_cell(another_board,blocks_allowed,temp_block):
			return blocks_allowed
		return [0,1,2,3,4,5,6,7,8]
	
	def game_win(self,temp_board,temp_block,cell,flag_player):
		if self.checkwin(temp_board,cell,flag_player):
			x = cell[0]
			y = cell[1]
			#print cell
			
			if ((0<=y) and (2>=y) and (0<=x) and (2>=x)):
				idb = 0
			if ((3<=y) and (5>=y) and (0<=x) and (2>=x)):
				idb = 1
			if ((6<=y) and (8>=y) and (0<=x) and (2>=x)):
				idb = 2
			if ((0<=y) and (2>=y) and (3<=x) and (5>=x)):
				idb = 3
			if ((3<=y) and (5>=y) and (3<=x) and (5>=x)):
				idb = 4
			if ((6<=y) and (8>=y) and (3<=x) and (5>=x)):
				idb = 5
			if ((0<=y) and (2>=y) and (6<=x) and (8>=x)):
				idb = 6
			if ((3<=y) and (5>=y) and (6<=x) and (8>=x)):
				idb = 7
			if ((6<=y) and (8>=y) and (6<=x) and (8>=x)):
				idb = 8
			if flag_player == 1:
				if temp_block[idb] == '-' or temp_block[idb] == 'x':
					temp_block[idb] = 'x'
			else:
				if temp_block[idb] == '-' or temp_block[idb] == 'o':
					temp_block[idb] = 'o'

			if temp_block[0] == 'x' and temp_block[1] == 'x' and temp_block[2] == 'x':
				return True
			if temp_block[0] == 'o' and temp_block[1] == 'o' and temp_block[2] == 'o':
				return True
			
			if temp_block[3] == 'x' and temp_block[4] == 'x' and temp_block[5] == 'x':
				return True
			if temp_block[3] == 'o' and temp_block[4] == 'o' and temp_block[5] == 'o':
				return True
			
			if temp_block[6] == 'x' and temp_block[7] == 'x' and temp_block[8] == 'x':
				return True
			if temp_block[6] == 'o' and temp_block[7] == 'o' and temp_block[8] == 'o':
				return True
			
			if temp_block[0] == 'x' and temp_block[3] == 'x' and temp_block[6] == 'x':
				return True
			if temp_block[0] == 'o' and temp_block[3] == 'o' and temp_block[6] == 'o':
				return True

			if temp_block[1] == 'x' and temp_block[4] == 'x' and temp_block[7] == 'x':
				return True
			if temp_block[1] == 'o' and temp_block[4] == 'o' and temp_block[7] == 'o':
				return True
			
			if temp_block[2] == 'x' and temp_block[5] == 'x' and temp_block[8] == 'x':
				return True
			if temp_block[2] == 'o' and temp_block[5] == 'o' and temp_block[8] == 'o':
				return True
			
			if temp_block[0] == 'x' and temp_block[4] == 'x' and temp_block[8] == 'x':
				return True
			if temp_block[0] == 'o' and temp_block[4] == 'o' and temp_block[8] == 'o':
				return True
			
			if temp_block[2] == 'x' and temp_block[4] == 'x' and temp_block[6] == 'x':
				return True
			if temp_block[2] == 'o' and temp_block[4] == 'o' and temp_block[6] == 'o':
				return True

	def move(self,temp_board,temp_block,old_move,flag):

		for_corner = [0,2,3,5,6,8]

		#List of permitted blocks, based on old move.
		blocks_allowed  = []

		if old_move[0] in for_corner and old_move[1] in for_corner:
			## we will have 3 representative blocks, to choose from

			if old_move[0] % 3 == 0 and old_move[1] % 3 == 0:
				## top left 3 blocks are allowed
				blocks_allowed = [0, 1, 3]
			elif old_move[0] % 3 == 0 and old_move[1] in [2, 5, 8]:
				## top right 3 blocks are allowed
				blocks_allowed = [1,2,5]
			elif old_move[0] in [2,5, 8] and old_move[1] % 3 == 0:
				## bottom left 3 blocks are allowed
				blocks_allowed  = [3,6,7]
			elif old_move[0] in [2,5,8] and old_move[1] in [2,5,8]:
				### bottom right 3 blocks are allowed
				blocks_allowed = [5,7,8]
			else:
				print "SOMETHING REALLY WEIRD HAPPENED!"
				sys.exit(1)
		else:
		#### we will have only 1 block to choose from (or maybe NONE of them, which calls for a free move)
			if old_move[0] % 3 == 0 and old_move[1] in [1,4,7]:
				## upper-center block
				blocks_allowed = [1]
	
			elif old_move[0] in [1,4,7] and old_move[1] % 3 == 0:
				## middle-left block
				blocks_allowed = [3]
		
			elif old_move[0] in [2,5,8] and old_move[1] in [1,4,7]:
				## lower-center block
				blocks_allowed = [7]

			elif old_move[0] in [1,4,7] and old_move[1] in [2,5,8]:
				## middle-right block
				blocks_allowed = [5]
			elif old_move[0] in [1,4,7] and old_move[1] in [1,4,7]:
				blocks_allowed = [4]

                for i in reversed(blocks_allowed):
                    if temp_block[i] != '-':
                        blocks_allowed.remove(i)
	# We get all the empty cells in allowed blocks. If they're all full, we get all the empty cells in the entire board.
		if flag == 'x':
			player_no = 1
		else:
			player_no =  2
		return self.AICALL(temp_board, blocks_allowed, temp_block, player_no,1)
